<?php
$VLANID_WAN = substr($_POST["VLANID_WAN"],0,3);
$TV_VLAN = $_POST["VLANID_TV"];
$ILE_TV = $_POST["ILE_TV"];
$VLANID_VOIP = $_POST["VLANID_TEL"];
$TEL_AKTYWNY = $_POST["TEL_AKTYWNY"];
$SSID_2g = $_POST["SSID_2g"];
$HASLO_2g = $_POST["HASLO_2g"];
$SSID_5g = $_POST["SSID_5g"];
$HASLO_5g = $_POST["HASLO_5g"];
$PORT_WAN = $_POST["PORT_WAN"];
$HASLO_ADMIN = $_POST["HASLO_ADMIN"];
$NAZWA_XML = $_POST["NAZWA_XML"];
 
$kanal_2g_1 = $_POST["2g_ch1"];
echo $kanal_2g_2 = $_POST["2g_ch2"];
$kanal_2g_3 = $_POST["2g_ch3"];
$kanal_2g_4 = $_POST["2g_ch4"];
$kanal_2g_5 = $_POST["2g_ch5"];
$kanal_2g_6 = $_POST["2g_ch6"];
$kanal_2g_7 = $_POST["2g_ch7"];
$kanal_2g_8 = $_POST["2g_ch8"];
$kanal_2g_9 = $_POST["2g_ch9"];
$kanal_2g_10 = $_POST["2g_ch10"];

$kanal_5g_1 = $_POST["5g_ch1"];
$kanal_5g_2 = $_POST["5g_ch2"];
$kanal_5g_3 = $_POST["5g_h3"];
$kanal_5g_4 = $_POST["5g_ch4"];
$kanal_5g_5 = $_POST["5g_ch5"];
$kanal_5g_6 = $_POST["5g_ch6"];
echo $kanal_5g_7 = $_POST["5g_ch7"];
$kanal_5g_8 = $_POST["5g_ch8"];
$kanal_5g_9 = $_POST["5g_ch9"];
$kanal_5g_10 = $_POST["5g_ch10"];

//DHCP - jeżeli TV aktywna to ETH3/4 - 1 (1 wyłącza przydzielanie dhcp)
$ETH2 = 0;
$ETH3 = 0;
$ETH4 = 0;
//MAPOWANIE PORTÓW KEY 0 - jeżeli TV aktywna to KEY_0_Ethernet_P3/4 WARTOSC - 0
$PORT_MAP_KEY_0_Ethernet_P2 = 1;
$PORT_MAP_KEY_0_Ethernet_P3 = 1;
$PORT_MAP_KEY_0_Ethernet_P4 = 1;
//MAPOWANIE PORTÓW KEY 1 - jeżeli TV aktywna to KEY_1_Ethernet_P3/4 WARTOSC - 1
$PORT_MAP_KEY_1_Ethernet_P2 = 0;
$PORT_MAP_KEY_1_Ethernet_P3 = 0;
$PORT_MAP_KEY_1_Ethernet_P4 = 0;

//KONFIGURACJA PORTÓW DLA IPTV
if($ILE_TV == 0)
{
	//jak tv włączone "1" na ETH (wyłącza dhcp gdzie 1)
	$ETH2 = 0;
	$ETH3 = 0;
	$ETH4 = 0;
	//jezeli tv to 0 
	$PORT_MAP_KEY_0_Ethernet_P2 = 1;
	$PORT_MAP_KEY_0_Ethernet_P3 = 1;
	$PORT_MAP_KEY_0_Ethernet_P4 = 1;
	//jezeli tv to 1
	$PORT_MAP_KEY_1_Ethernet_P2 = 0;
	$PORT_MAP_KEY_1_Ethernet_P3 = 0;
	$PORT_MAP_KEY_1_Ethernet_P4 = 0;
} elseif($ILE_TV == 1) {
	//jak tv włączone "1" na ETH (wyłącza dhcp gdzie 1)
	$ETH2 = 0;
	$ETH3 = 0;
	$ETH4 = 1;
	//jezeli tv to 0 
	$PORT_MAP_KEY_0_Ethernet_P2 = 1;
	$PORT_MAP_KEY_0_Ethernet_P3 = 1;
	$PORT_MAP_KEY_0_Ethernet_P4 = 0;
	//jezeli tv to 1
	$PORT_MAP_KEY_1_Ethernet_P2 = 0;
	$PORT_MAP_KEY_1_Ethernet_P3 = 0;
	$PORT_MAP_KEY_1_Ethernet_P4 = 1;
} elseif($ILE_TV == 2) {
	//jak tv włączone "1" na ETH (wyłącza dhcp gdzie 1)
	$ETH2 = 0;
	$ETH3 = 1;
	$ETH4 = 1;
	//jezeli tv to 0 
	$PORT_MAP_KEY_0_Ethernet_P2 = 1;
	$PORT_MAP_KEY_0_Ethernet_P3 = 0;
	$PORT_MAP_KEY_0_Ethernet_P4 = 0;
	//jezeli tv to 1
	$PORT_MAP_KEY_1_Ethernet_P2 = 0;
	$PORT_MAP_KEY_1_Ethernet_P3 = 1;
	$PORT_MAP_KEY_1_Ethernet_P4 = 1;
} elseif($ILE_TV == 3) {
	//jak tv włączone "1" na ETH (wyłącza dhcp gdzie 1)
	$ETH2 = 1;
	$ETH3 = 1;
	$ETH4 = 1;
	//jezeli tv to 0 
	$PORT_MAP_KEY_0_Ethernet_P2 = 0;
	$PORT_MAP_KEY_0_Ethernet_P3 = 0;
	$PORT_MAP_KEY_0_Ethernet_P4 = 0;
	//jezeli tv to 1
	$PORT_MAP_KEY_1_Ethernet_P2 = 1;
	$PORT_MAP_KEY_1_Ethernet_P3 = 1;
	$PORT_MAP_KEY_1_Ethernet_P4 = 1;
} else {
	echo "BŁĄD";
}

//KONFIGURACJA VOIP
if($TEL_AKTYWNY == "Yes")
{
	$NUMER_TEL = $_POST["NUMER_TEL"];
	$HASLO_TEL = $_POST["HASLO_TEL"];
} else {
	$NUMER_TEL = "";
	$HASLO_TEL = "";
} 

//POBIERZ PLIK Z SERWERA
$xml_string = file_get_contents("dasan.xml");
$root = simplexml_load_string($xml_string);

//KONFIGURACJA VLAN INTERNET
$root->AdvacnedConfiguration->WANConnectionsConfigurations->DictionaryEntry[0]->Value->VLANID = $VLANID_WAN;
//KONFIGURACJA VLAN IPTV
$root->AdvacnedConfiguration->WANConnectionsConfigurations->DictionaryEntry[1]->Value->VLANID = $TV_VLAN;
//KONFIGURACJA VLAN VOIP
$root->AdvacnedConfiguration->WANConnectionsConfigurations->DictionaryEntry[2]->Value->VLANID = $VLANID_VOIP;
//KONFIGURACJA ON/OFF VOIP
$root->AdvacnedConfiguration->WANConnectionsConfigurations->DictionaryEntry[2]->Value->Active = $TEL_AKTYWNY;
//KONFIGURACJA DHCP NA POTRZEBY IPTV - 1 GDZIE TV AKTYWNE
$root->AdvacnedConfiguration->LANConectionsConfiguration->DHCPOption->DictionaryEntry[0]->Value->ETH2 = $ETH2;
$root->AdvacnedConfiguration->LANConectionsConfiguration->DHCPOption->DictionaryEntry[0]->Value->ETH3 = $ETH3;
$root->AdvacnedConfiguration->LANConectionsConfiguration->DHCPOption->DictionaryEntry[0]->Value->ETH4 = $ETH4;
//KONFIGURACJA MAPOWANIA PORTÓW NA POTRZEBY IPTV - KEY 0
$root->FirewallConfiguration->PortMappingConfiguration->PortMappingGroup->DictionaryEntry[0]->Value->Ethernet_P2 = $PORT_MAP_KEY_0_Ethernet_P2;
$root->FirewallConfiguration->PortMappingConfiguration->PortMappingGroup->DictionaryEntry[0]->Value->Ethernet_P3 = $PORT_MAP_KEY_0_Ethernet_P3;
$root->FirewallConfiguration->PortMappingConfiguration->PortMappingGroup->DictionaryEntry[0]->Value->Ethernet_P4 = $PORT_MAP_KEY_0_Ethernet_P4;
//KONFIGURACJA MAPOWANIA PORTÓW NA POTRZEBY IPTV - KEY 1
$root->FirewallConfiguration->PortMappingConfiguration->PortMappingGroup->DictionaryEntry[1]->Value->Ethernet_P2 = $PORT_MAP_KEY_1_Ethernet_P2;
$root->FirewallConfiguration->PortMappingConfiguration->PortMappingGroup->DictionaryEntry[1]->Value->Ethernet_P3 = $PORT_MAP_KEY_1_Ethernet_P3;
$root->FirewallConfiguration->PortMappingConfiguration->PortMappingGroup->DictionaryEntry[1]->Value->Ethernet_P4 = $PORT_MAP_KEY_1_Ethernet_P4;
//KONFIGURACJA WIFI 2G SSID/HASŁO
$root->WifiConfiguration->SSIDConfiguration->DictionaryEntry[0]->Value->SSID = $SSID_2g;
$root->WifiConfiguration->SSIDConfiguration->DictionaryEntry[0]->Value->WPAPSK = $HASLO_2g;
//KONFIGURACJA WIFI 5G SSID/HASŁO
$root->Wifi5GConfiguration->SSIDConfiguration->DictionaryEntry[0]->Value->SSID = $SSID_5g;
$root->Wifi5GConfiguration->SSIDConfiguration->DictionaryEntry[0]->Value->WPAPSK = $HASLO_5g;
//KONFIGURACJA VOIP
$root->VoIPConfiguration->sip_information_config->sip_user_data_configs->DictionaryEntry[0]->Value->user_data_userpart_aor = $NUMER_TEL;
$root->VoIPConfiguration->sip_information_config->sip_user_data_configs->DictionaryEntry[0]->Value->user_data_displayname = $NUMER_TEL;
$root->VoIPConfiguration->sip_information_config->sip_user_data_configs->DictionaryEntry[0]->Value->user_data_auth_security_username = $NUMER_TEL;
$root->VoIPConfiguration->sip_information_config->sip_user_data_configs->DictionaryEntry[0]->Value->user_data_auth_security_password = $HASLO_TEL;
//*****ZAAWANSOWANE*****
//HASŁO ADMIN
$root->MaintenanceConfiguration->AdministrationConfiguration->AdminPassword = $HASLO_ADMIN;
//PORT KONFIGURACYJNY
$root->MaintenanceConfiguration->WebPort = $PORT_WAN;
//**********************

//ZAPISZ XML
$root->asXml($NAZWA_XML.".xml");

//POBIERANIE XML NA DYSK
header("Content-disposition: attachment; filename=".$NAZWA_XML.".xml");
header("Content-type: application/xml");
readfile($NAZWA_XML.".xml");
//KASOWANIE PLIKU Z SERWERA
unlink($NAZWA_XML.".xml");

?>